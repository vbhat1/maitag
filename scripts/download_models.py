# download_models.py

ENCODER = 'sbert'

if 'spacy' in ENCODER:
    # https://spacy.io
    import spacy
    nlp = 'en_core_web_md'
    try:
        nlp = spacy.load(nlp)
    except OSError:
        spacy.cli.download(nlp)
    nlp = spacy.load(nlp) if isinstance(nlp, str) else nlp
    def encoder(s):
        return nlp(s).vector
if 'sbert' in ENCODER:
    # https://www.sbert.net/
    from sentence_transformers import SentenceTransformer
    sbert = SentenceTransformer('paraphrase-MiniLM-L6-v2')
    def encoder(s):
        return sbert.encode(s)


# print(len(encoder("Hello world.")))
