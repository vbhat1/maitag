#!/usr/bin/env bash

# Exit on error
set -o errexit

# Install dependencies
pip install -r requirements_dev.txt 

# Serve static files
python manage.py collectstatic --no-input

# Make and apply migrations
python manage.py makemigrations
python manage.py migrate

# Create a superuser
./scripts/create_superuser.sh

python ./scripts/download_models.py
