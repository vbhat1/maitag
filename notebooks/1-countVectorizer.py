# Dependencies
from sklearn.feature_extraction.text import CountVectorizer 

corpus = [
    'This is the first document.',
    'This document is the second document.',
    'And this is the third one.',
    'Is this the first document?',
]

# Initialize the CountVectorizer() object 
vectorizer = CountVectorizer()
# Fit our document to preprocess it, tokenize it, and vectorize it
X = vectorizer.fit_transform(corpus) 

# Vocabulary and BOW 
print(vectorizer.get_feature_names_out())
print(X.toarray())

# I should read the ngram section in NLPiA to understanf ngram
vectorizer2 = CountVectorizer(analyzer = 'word', ngram_range = (2, 2))
X2 = vectorizer2.fit_transform(corpus)

print(vectorizer2.get_feature_names_out())
print(X2.toarray())

# Split the given document into units or tokens
sentence = "we are the world, the champions, and the leaders"
default_tokenizer_function = vectorizer.build_tokenizer()
words = default_tokenizer_function(sentence)
print(words)

# 






