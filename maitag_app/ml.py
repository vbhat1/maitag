import numpy as np
import os
import pickle
from django.core.cache import cache
from scripts.download_models import *



os.environ['DJANGO_SETTINGS_MODULE'] = 'maitag_site.settings'

# set and get your trained model from the cache
model_cache_key = 'model_cache'
model_path = 'staticfiles/maitag_app/trained_models/model_v1.pkl'
# get model from cache
model = cache.get(model_cache_key)

if model is None:
    # the model isn't in the cache
    with open(model_path, 'rb') as file:
        model = pickle.load(file)
        model.class_names = list(model.classes_)
        # set the model in the cache and save it
        cache.set(model_cache_key, model, None) # None is the timeout parameter. It means cache forever

with open(model_path, 'rb') as file:
    model = pickle.load(file)
    model.class_names = list(model.classes_)


def predict_labels(utterances):
    """ Use pretrained LogisticRegression and SBERT to predict intent labels

    >>> predict_labels(['bye', 'hello world!'])
    [
     {'label': 'goodbye', 'confidence': 0.6666973673705877},
     {'label': 'hello', 'confidence': 0.6213821510363821}
    ]
    """
    single = False
    if isinstance(utterances, str):
        single = True
        utterances = [utterances]
    encoded_utterance = sbert.encode(utterances)
    labels = model.predict(encoded_utterance)
    # label = list(prediction)[0]
    # class_index = np.where(model.classes_ == label)
    probas = model.predict_proba(encoded_utterance)
    predictions = []
    for label, ps in zip(labels, probas):
        class_num = model.class_names.index(label)
        predictions.append([label, ps[class_num]])
    predictions = [
        dict(label=lbl, confidence=conf) for lbl, conf in predictions]
    if single:
        return predictions[0]
    return predictions


if __name__== '__main__':
     predict_labels(['tell me more about it', 'how was your day?', 'can I ask you something?', 'how are you?'])
