from django.db import models
from users.models import CustomUser
from itertools import chain


class MLModel(models.Model):
    """represents the used machine learning algorithm"""

    name = models.CharField(max_length = 30)
    path = models.CharField(max_length = 256, null = True)

    def __str__(self):
        return self.name


class Classifier(models.Model):
    """represents the tagger"""

    name = models.CharField(max_length = 30)
    mlmodel = models.ForeignKey(MLModel, on_delete = models.CASCADE)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.name

class Intent(models.Model):

    name = models.CharField(max_length = 30)

    def __str__(self):
        return self.name


class LabelInstance(models.Model):
    """represents massage tag (intent)"""

    intent = models.ForeignKey(Intent, null = True, on_delete = models.CASCADE)
    description = models.TextField()
    classifier = models.ForeignKey(Classifier, null = True, on_delete = models.CASCADE)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.description


class Utterance(models.Model):
    """represents user messages"""

    user = models.ForeignKey(CustomUser, on_delete = models.CASCADE)
    text = models.TextField()
    intents = models.ManyToManyField(Intent, blank = True)
    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)

    def __str__(self):
        return self.text


class Project(models.Model):

    project_id = models.CharField(max_length = 256, null = True)
    utterance_id = models.CharField(max_length = 256, null = True)
    utterance = models.CharField(max_length = 256)
    label = models.CharField(max_length = 100, null = True)
    confidence = models.IntegerField(null = True)

    def __str__(self):
        return self.utterance




