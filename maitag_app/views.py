from django.http import JsonResponse
from django.shortcuts import render
from django.urls import reverse
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import (
    Utterance, 
    Intent, 
    LabelInstance, 
    MLModel, 
    Classifier, 
    Project
)
from .ml import *
from .serializers import (
    UtteranceSerializer, 
    IntentSerializer, 
    LabelInstanceSerializer,
    MLModelSerializer,
    ClassifierSerializer,
    ProjectSerializer
)



def home(request):
    return render(request, 'maitag_app/home.html')


class MLModelList(generics.ListCreateAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer


class MLModelDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = MLModel.objects.all()
    serializer_class = MLModelSerializer  


class ClassifierList(generics.ListCreateAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class ClassifierDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Classifier.objects.all()
    serializer_class = ClassifierSerializer


class IntentList(generics.ListCreateAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class IntentDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Intent.objects.all()
    serializer_class = IntentSerializer


class LabelInstanceList(generics.ListCreateAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class LabelInstanceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = LabelInstance.objects.all()
    serializer_class = LabelInstanceSerializer


class UtteranceList(generics.ListCreateAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer


class UtteranceDetail(generics.RetrieveUpdateDestroyAPIView):

    queryset = Utterance.objects.all()
    serializer_class = UtteranceSerializer



class ProjectList(generics.ListAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer   


class ProjectDetail(generics.RetrieveAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer


def predict_unknown_label(utterance=None, **kwargs):
    """label unknown utterance intent using clustering 

    >>> predict_unknown_label(utterance="text never seen before")
    {'label': 'text', ...}
    """
    answer = {
        "label": utterance.split()[0],
        "confidence": 1.0,
    }
    return answer


class Label(generics.CreateAPIView):

    queryset = Project.objects.all()
    serializer_class = ProjectSerializer

    def post(self, request):
        serializer = ProjectSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            utterance = serializer.data['utterance']
            predictions = predict_labels(utterance)
            data = {
                "project_id": serializer.data['project_id'],
                "utterance_id": serializer.data['utterance_id'],
                "utterance": utterance,
                "label": predictions['label'],
                "confidence": round(predictions['confidence'], 2)
            }
            if data['label'] == 'unknown':
                data_update = predict_unknown_label(**data)
                data.update(data_update)
            return Response(data, status = status.HTTP_200_OK)
        return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)




""" Concrete View Classes
# CreateAPIView
Used for create-only endpoints.
# ListAPIView
Used for read-only endpoints to represent a collection of model instances.
# RetrieveAPIView
Used for read-only endpoints to represent a single model instance.
# DestroyAPIView
Used for delete-only endpoints for a single model instance.
# UpdateAPIView
Used for update-only endpoints for a single model instance.
# ListCreateAPIView
Used for read-write endpoints to represent a collection of model instances.
RetrieveUpdateAPIView
Used for read or update endpoints to represent a single model instance.
# RetrieveDestroyAPIView
Used for read or delete endpoints to represent a single model instance.
# RetrieveUpdateDestroyAPIView
Used for read-write-delete endpoints to represent a single model instance.
"""