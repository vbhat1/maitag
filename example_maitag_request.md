
```python
import requests
```
>>> requests.get?

>>> url = 'https://maitag.onrender.com/api/maitag/label/'

>>> data = {"utterance": "how was your day"}
>>> response_1 = requests.post(url, json=data)
<Response [200]>
>>> response_2 = requests.post(url, json=data)
>>> response_2.text
'{"project_id":null,"utterance_id":null,"utterance":"how was your day","label":"how_are_you","confidence":0.98}'
>>> data = {"utterance": "rochdi is broken"}
>>> response_3 = requests.post(url, json=data)
>>> response_3.text
'{"project_id":null,"utterance_id":null,"utterance":"rochdi is broken","label":"unknown","confidence":0.93}'

>>> hist -o
>>> hist -o -p
>>> hist -o -p -f example_maitag_request.md
