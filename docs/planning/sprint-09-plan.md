# Sprint 9 (Oct 14 - Oct 21)

* [x] R0.1-0.1: Create a new branch called `fix-eof-bug` to fix the EOF bug
* [x] R1-0.8: Fix the EOF bug using this [StackOverflow](https://stackoverflow.com/a/43170530/623735) post
* [x] R0.5-0.5: Test the EOF bugfix in production
* [x] R0.5-0.2: Create a pull/merge request and assign it to Hobson
* [x] R1-0.5: Create one endpoint with the five fields Hobson has mentioned
* [x] R1-0.5: Use the Predict view to reply with the predicted/suggested intent
* [x] R0.5-0.5: Store the prediction requests in `Project` table
* [x] R0.2-0.1: Delete the `Prediction` table and its components
* [x] R0.2-0.2: Test the endpoint using the Python requests library
* [x] R0.5-0.5: Format the markdow file that includes the test and send it to Maria
* [x] R0.5-0.2: Create a Home view with template
* [x] R0.2-0.5: Create a header in the home page that include the authentication links
* [x] R0.5-0.5: Add a main section that contains the link to the key endpoint `/label`
* [x] R0.2-0.5: Add a left sidebar that includes the links to other MAITAG API endpoints
* [x] R0.2-0.2: Create an issue to address a new feature that modularize the `predict_labels()` function
* [x] R0.5-0.1: Create a new branch called `modularize-predict` 

## Done: Sprint 8 (Oct 07 - Oct 14)

* [x] R2-2: Convert the current database models to RESTful API
* [x] R1-1: Test the REST API locally and in production
* [x] H0.5: Have Hobson send you the utterance dictionary (JSON)
* [x] R1-0.5: Create Post endpoint for receiving the utterance dictionary (JSON)
* [x] R1-0.5: Reply to Post request with a hardcoded label
* [x] R0.5-0.2: Create a new table called `Project` 
* [x] R0.2-0.2: Add one-to-many relationship between `Utterance` and `Project` tables
* [x] R1-0.2: Add a cell to save and load the model in MOIA Jupyter notebook
* [x] R0.2-0.1: Save a trained Classifier in the Django application (static directory)
* [x] R0.2-0.1: Deploy the model to Render
* [x] R1-0.5: Create a new file called `ml.py` and load the model for the Rest endpoint using `pickle.load()`
* [x] R1-1: Create a function in `ml.py` that uses model.predict() on utterances to return predictions in a list
* [x] R0.2-0.2: Import the ml.predict() function within the predict view
* [x] R1-0.5: Reply with the predicted label using the saved model
* [x] R0.5-0.5: Store `predict()` requests in `Project` table
* [x] R1-0.5: Create a new table called `Prediction`
* [x] R1-0.5: Store `predict()` responses in `Prediction` table