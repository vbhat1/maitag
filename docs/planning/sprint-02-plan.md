# Sprint 2 (Jul 22 - Jul 29)

* [x] R1-1.5: Sketch up an initial database diagram using draw.io
* [x] R1-0.2: Create a model named **Tag** to represent message tag (intent/label)
* [x] R1-0.2: Create a model named **Message** to represent user utterances
* [x] R0.2-0.2: Add many-to-many between **Message** and **Tag** tables
* [x] R1-0.5: Create two models named **Tagger** and **MLModel** to represent the classifier
* [x] R0.5-0.2: Add one-to-many between **Tagger** and **Tag** to represent the source of the tag

## Done: Sprint 1 (Jul 15 - Jul 22)

* [x] R1-1: Created an Initial Django project with users app
* [x] R1-1: Created a Custom User model based on BaseUserManager/AbstractBaseUser
* [x] R1-1: Created a Profile model that inherits from User table
* [x] R1-1: Added signals to create profiles automatically once the User is created
* [x] R1-1: Customized the admin interface for CRUDing accounts/profiles
* [x] R1-1: Configure JWT-based authentication settings using `dj-rest-auth`
* [x] R1-1: Transform the CustomUser and Profile models into a Restful API
* [x] R1-1: Create a custom API endpoint to register new users
* [x] R1-1: Implement the authentication (login/logout) endpoints
* [x] R1-1: Create and activate a custom API endpoint to reset password


