# Sprint 3 (Sep 02 - Sep 09)

* [x] R1-2: Add some learning tasks to read chapter 2 in **NLPiA**
    * [x] R2-1.5: Read and understand **2.2 Building your vocabulary with a tokenizer**
    * [x] R1-0.5: Read and understand **2.2.1 Dot product**
* [x] R1-1.5: Do an exercise to create BOW vectors for each document in the MAITAG (Maya) dataset of anonymized utterances
* [] H?: Create tokenization exercise instructions on how to use ipython
* [] H?: Check the answer with doctest and use hist to export your work
* [] H?: Create a vectorization exercise
* [] H?: Create `team/exercises/nlpia/` dir for all the NLP exercies

## Done: Sprint 2 (Jul 22 - Jul 29)

* [x] R1-1.5: Sketch up an initial database diagram using draw.io
* [x] R1-0.2: Create a model named **Tag** to represent message tag (intent/label)
* [x] R1-0.2: Create a model named **Message** to represent user utterances
* [x] R0.2-0.2: Add many-to-many between **Message** and **Tag** tables
* [x] R1-0.5: Create two models named **Tagger** and **MLModel** to represent the classifier
* [x] R0.5-0.2: Add one-to-many between **Tagger** and **Tag** to represent the source of the tag


