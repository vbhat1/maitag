# Sprint 11 (Oct 28 - Nov 04)

* [x] R1-0.5: Create a README file for `maitag`
* [] R1: Add an automatic test to test the `predict_unknown_label()` during the build
* [x] R2-2: Take another practical tutorial to get familiar with ToolJet
* [x] R2-1: Send an API request to `/api/maitag/label` endpoint when the user assigns an intent
* [] R0.1: Create a branch called `feature-rapidpro-messages` 
* [] R2: Create a django app as part of `django-delvin` that regularly syncs new messages from a TextIt/RapidPro bot, check this [link](https://gitlab.com/tangibleai/team/-/blob/main/exercises/2-plan-your-prosocial-ai-project/prosocial-ai-project-ideas-tangibleai-priorities.yml#L13) for more details
* [] R0.5: Create a pull/merge request that highlights the feature and assign it to Hobson
* [] R1: Modularize the `predict_label()` to have two functions that predict one single utterance and multiple utterances
* [] R0.5: Create a pull/merge request to merge the feature and assign it to Hobson
* [] H0.2: Have Hobson send you the sendgrid tokens via Bitwarden

## Done: Sprint 10 (Oct 21 - Oct 28)

* [x] R2-2: Merge the authentication API endpoints into the `django-delvin/users` app
* [x] R1-1: Merge the API endpoints of `maitag` into the `django-delvin/maitag` app
* [x] R0.5-0.2: Add the `maitag`'s home page templates to `django-delvin/maitag` app
* [x] R1-0.5: Test the API endpoints in production
* [x] R0.5-0.1: Contact Maria for the tooljet/javascript widget frontend task
* [x] R2-2.2: Take a tutorial on ToolJet to get familiar with the framework
* [x] R0.5-0.2: Connect to Maria's database using the credentials on Bitwarden
* [x] R1-0.9: Browse the database and understand its structure
* [x] R1-0.8: Read and understand the [feature machine assistance](https://gitlab.com/tangibleai/maitag/-/blob/main/docs/feature-machine-assistance.md) doc
* [x] R0.5-0.1: Schedule a meeting with Vish to discuss `maitag` and next features