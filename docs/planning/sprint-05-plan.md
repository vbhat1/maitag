# Sprint 5 (Sep 16 - Sep 23)

### MOIA notebook

* [x] R0.1: Copy Hobson's Jupyter notebook that process MAYA messages
* [x] R2-2: Run and understand what each cell is doing in the notebook

## Done: Sprint 4 (Sep 09 - Sep 16)

* [x] R2-1: Read the Scikit-learn documentation on `CountVectorizer()` 
* [x] R2-0.8: Do practice understanding how to count vectors with Scikit-learn 
* [x] R1-0.5: Count vectors for all MAYA messages that are included in the `maitag/docs` folder
* [x] R1-1: Write an explanation on how to use `CountVectorizer` in Scikit-learn
* [x] R0.1-0.1: Push the explanation to the `team` repository
* [x] R1-1: Do some research to understand what Pseudo code is
* [x] R1-2: Write a document on Pseudo code and push it to the `team` repo 
* [x] R1-1: Write a pseudo code on `CountVectorizer()` in Sklearn and push it to the `team` repo
* [x] R0.5-0.2: Search for Hobson's Jupyter notebook that processes MAYA messages
* [x] R1-1.5: Install the dependencies and run the notebook