# Sprint 8 (Oct 07 - Oct 14)

* [x] R2-2: Convert the current database models to RESTful API
* [x] R1-1: Test the REST API locally and in production
* [x] H0.5: Have Hobson send you the utterance dictionary (JSON)
* [] H0.5: Find sendgrid tokens in Bitwarden within Tangible AI vault
* [x] R1-0.5: Create Post endpoint for receiving the utterance dictionary (JSON)
* [x] R1-0.5: Reply to Post request with a hardcoded label
* [x] R0.5-0.2: Create a new table called `Project` 
* [x] R0.2-0.2: Add one-to-many relationship between `Utterance` and `Project` tables
* [x] R1-0.2: Add a cell to save and load the model in MOIA Jupyter notebook
* [x] R0.2-0.1: Save a trained Classifier in the Django application (static directory)
* [x] R0.2-0.1: Deploy the model to Render
* [x] R1-0.5: Create a new file called `ml.py` and load the model for the Rest endpoint using `pickle.load()`
* [x] R1-1: Create a function in `ml.py` that uses model.predict() on utterances to return predictions in a list
* [x] R0.2-0.2: Import the ml.predict() function within the predict view
* [x] R1-0.5: Reply with the predicted label using the saved model
* [x] R0.5-0.5: Store `predict()` requests in `Project` table
* [x] R1-0.5: Create a new table called `Prediction`
* [x] R1-0.5: Store `predict()` responses in `Prediction` table

## Done: Sprint 7 (Sep 30 - Oct 07)

* [x] R0.2-0.2: Overwrite/edit the original Hobson's MAYA notebook 
* [x] R2-1: Update the initial database diagram 
* [x] R0.5-0.2: Alter/Update the database tables based on the database diagram
* [x] R0.1-0.1: Add project dependencies to **requirements.txt**
* [x] R0.1-0.1: Create a build script for render deployement
* [x] R0.2-0.2: Create another script to create a superuser on production
* [x] R0.5-0.2: Create a web service on Render to deploy `MAITAG`
* [x] R0.2-0.1: Add environement variables needed for the web service
* [x] R0.2-0.1: Push the changes and deploy 
* [x] R1-0.8: Setup the configuration needed to serve the static files in production
* [x] R2-1.2: Fix the CSS issues with the Django admin site. 
* [x] R1-0.2: Test the app and the authentication system in production