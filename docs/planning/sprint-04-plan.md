# Sprint 4 (Sep 09 - Sep 16)

* [x] R2-1: Read the Scikit-learn documentation on `CountVectorizer()` 
* [x] R2-0.8: Do practice understanding how to count vectors with Scikit-learn 
* [x] R1-0.5: Count vectors for all MAYA messages that are included in the `maitag/docs` folder
* [x] R1-1: Write an explanation on how to use `CountVectorizer` in Scikit-learn
* [x] R0.1-0.1: Push the explanation to the `team` repository
* [x] R1-1: Do some research to understand what Pseudo code is
* [x] R1-2: Write a document on Pseudo code and push it to the `team` repo 
* [x] R1-1: Write a pseudo code on `CountVectorizer()` in Sklearn and push it to the `team` repo
* [x] R0.5-0.2: Search for Hobson's Jupyter notebook that processes MAYA messages
* [x] R1-1.5: Install the dependencies and run the notebook

## Done: Sprint 3 (Sep 02 - Sep 09)

* [x] R1-2: Add some learning tasks to read chapter 2 in **NLPiA**
    * [x] R2-1.5: Read and understand **2.2 Building your vocabulary with a tokenizer**
    * [x] R1-0.5: Read and understand **2.2.1 Dot product**
* [x] R1-1.5: Do an exercise to create BOW vectors for each document in the MAITAG (Maya) dataset of anonymized utterances