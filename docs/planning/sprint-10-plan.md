# Sprint 10 (Oct 21 - Oct 28)

* [x] R2-2: Merge the authentication API endpoints into the `django-delvin/users` app
* [x] R1-1: Merge the API endpoints of `maitag` into the `django-delvin/maitag` app
* [x] R0.5-0.2: Add the `maitag`'s home page templates to `django-delvin/maitag` app
* [x] R1-0.5: Test the API endpoints in production
* [x] R0.5-0.1: Contact Maria for the tooljet/javascript widget frontend task
* [x] R2-2.2: Take a tutorial on ToolJet to get familiar with the framework
* [x] R0.5-0.2: Connect to Maria's database using the credentials on Bitwarden
* [x] R1-0.9: Browse the database and understand its structure
* [x] R1-0.8: Read and understand the [feature machine assistance](https://gitlab.com/tangibleai/maitag/-/blob/main/docs/feature-machine-assistance.md) doc
* [x] R0.5-0.1: Schedule a meeting with Vish to discuss `maitag` and next features
* [] R0.2: <del> Create a PostgreSQL database service on Render </del>
* [] R0.2: <del> Connect the database service to `MAITAG` </del>
* [] R0.2: <del> Create a superuser account for Tangible AI </del>

## Done: Sprint 09 (Oct 14 - Oct 21)

* [x] R0.1-0.1: Create a new branch called `fix-eof-bug` to fix the EOF bug
* [x] R1-0.8: Fix the EOF bug using this [StackOverflow](https://stackoverflow.com/a/43170530/623735) post
* [x] R0.5-0.5: Test the EOF bugfix in production
* [x] R0.5-0.2: Create a pull/merge request and assign it to Hobson
* [x] R1-0.5: Create one endpoint with the five fields Hobson has mentioned
* [x] R1-0.5: Use the Predict view to reply with the predicted/suggested intent
* [x] R0.5-0.5: Store the prediction requests in `Project` table
* [x] R0.2-0.1: Delete the `Prediction` table and its components
* [x] R0.2-0.2: Test the endpoint using the Python requests library
* [x] R0.5-0.5: Format the markdow file that includes the test and send it to Maria
* [x] R0.5-0.2: Create a Home view with template
* [x] R0.2-0.5: Create a header in the home page that include the authentication links
* [x] R0.5-0.5: Add a main section that contains the link to the key endpoint `/label`
* [x] R0.2-0.5: Add a left sidebar that includes the links to other MAITAG API endpoints
* [x] R0.2-0.2: Create an issue to address a new feature that modularize the `predict_labels()` function
* [x] R0.5-0.1: Create a new branch called `modularize-predict` 