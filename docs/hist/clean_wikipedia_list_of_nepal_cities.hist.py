pd.read_html('https://en.wikipedia.org/wiki/List_of_cities_in_Nepal')
import pandas as pd
dfs = pd.read_html('https://en.wikipedia.org/wiki/List_of_cities_in_Nepal')
dfs[0]
dfs[1]
dfs[1]
dfs[2]
dfs[3]
dfs[4]
['Name' in df.columns for df in dfs]
['Name' in df.columns or 'City' in df.columns for df in dfs]
sum(['Name' in df.columns or 'City' in df.columns for df in dfs])
dfs = [df for df in dfs if 'name' in df.columns.str.lower() or 'city' in df.columns.str.lower()]
dfs = [df for df in dfs if 'Name' in df.columns or 'City' in df.columns]
df = pd.concat(dfs)
df['City'].isna().sum()
df['Name'].isna().sum()
len(df['Name'])
293+103
df['Name'].fillna(df['City'][df['Name'].isna()])
name_isna = df.Name.isna()
df['Name'][name_isna] = list(df['City'][name_isna])
df['Name'].loc[name_isna] = list(df['City'][name_isna].copy())
df['Name'].iloc[name_isna] = list(df['City'][name_isna].copy())
names = []
for i, row in df.iterrows():
    names.append(row['Name'] if not pd.isna(row['Name']) else row['City'])
df['Name'] = names
df['is_city'] = name_isna
df.columns = [str(c).lower().replace(' ', '_').strip() for c in df.columns]
df
df.columns = [str(c).lower().replace(' ', '_').replace('(', '').replace(')', '').strip() for c in df.columns]
df
df.columns
df['population']
df['population'] = df[[c for c in df.columns if 'population_2011' in c]].T.sum()
df[[c for c in df.columns if 'population_2011' in c]]
df[[c for c in df.columns if 'population_2011' in c]].T
df[[c for c in df.columns if 'population_2011' in c]].T.sum()
df[[c for c in df.columns if 'population' in c]].T.sum()
population = df[[c for c in df.columns if 'population' in c]].fillna('0').T.sum()
population = df[[c for c in df.columns if 'population' in c]].fillna('0')
population
for c in population.columns:
    df[c] = [x if np.isnumeric(x) else 0 for x in df[c]]
import numpy as np
population.info()
pd.to_numeric(population)
pops = pd.DataFrame()
for c in population.columns:
    pops[c] = pd.to_numeric(df[c])
pops = pd.DataFrame()
for c in population.columns:
    s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
    pops[c] = pd.to_numeric(s)
pops = pd.DataFrame()
for c in population.columns:
    s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
    s = [np.nan if x == 'nan' else x]
    pops[c] = pd.to_numeric(s)
pops = pd.DataFrame()
for c in population.columns:
    s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
    s = [np.nan if x == 'nan' else x for x in s]
    pops[c] = pd.to_numeric(s)
pops
pops.T.sum().T
df['population'] = pops.T.sum().T
df
df.drop(population.columns)
newcols = [c for c in df.columns if not c.startswith('population_')]
dfnew = df[newcols].copy()
dfnew
dfnew.columns
df = dfnew
df['city']
df['name']
df = df.drop('city', axis=1)
df.columns
hist
def combine_numeric_columns(data, prefix):
    columns = [str(c).lower().strip() data.columns if str(c).lower().strip().startswith(prefix)]
def combine_numeric_columns(data, prefix):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
columns
def combine_numeric_columns(data, prefix):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    return columns
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    return data[columns]
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    newdf = getattr(newdf.T, operation)().T
    return newdf
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.') for x in s]
        s = [x.replace('.', n=1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
'00.000.00'.replace?
help(str.replace)
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.') for x in s]
        s = [x.replace('.', count=1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
combine_numeric_columns(df, 'density')
from collections import Counter
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', count=1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', count=1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    data = data.copy()
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
    data = data.drop(newdf.columns)
    data[prefix] = newcol
    return data
combine_numeric_columns(df, 'density')
df.columns
def combine_numeric_columns(data, prefix, operation='sum'):
    data = data.copy()
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
    data = data.drop(list(newdf.columns))
    data[prefix] = newcol
    return data
combine_numeric_columns(df, 'density')
def combine_numeric_columns(data, prefix, operation='sum'):
    data = data.copy()
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
    for c in  newdf.columns:
        data = data.drop(c, axis=1)
    data[prefix] = newcol
    return data
def combine_numeric_columns(data, prefix, operation='sum'):
    data = data.copy()
    columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
    newdf = pd.DataFrame()
    for c in columns:
        s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
        counts = [Counter(x).get('.', 0) for x in s]
        print(pd.Series(pd.Series(counts) > 1).sum())
        s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
        s = [np.nan if x == 'nan' else x for x in s]
        newdf[c] = pd.to_numeric(s)
    newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
    data = data.drop(newdf.columns, axis=1)
    data[prefix] = newcol
    return data
combine_numeric_columns(df, 'density')
combine_numeric_columns(df, 'density').describe()
mkdir ~/.nlpia2-data/nepal/
hist -f ~/.nlpia2-data/nepal/clean_wikipedia_list_of_nepal_cities.hist.py
hist -o -p -f ~/.nlpia2-data/nepal/clean_wikipedia_list_of_nepal_cities.hist.md
pwd
ls data
hist -o -p -f data/clean_wikipedia_list_of_nepal_cities.hist.md
hist -f data/clean_wikipedia_list_of_nepal_cities.hist.py
