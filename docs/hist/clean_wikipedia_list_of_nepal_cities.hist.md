>>> pd.read_html('https://en.wikipedia.org/wiki/List_of_cities_in_Nepal')
>>> import pandas as pd
>>> dfs = pd.read_html('https://en.wikipedia.org/wiki/List_of_cities_in_Nepal')
>>> dfs[0]
                                                   0
0                This article is part of a series on
1                  Administrative divisions of Nepal
2  Federal Democratic Republic of Nepal (since 2015)
3  Provinces: 7 (First level) Province No. 1 Madh...
4  Districts: 77 (Second level) Districts of Prov...
5  Local-level body: 753 (Third level) Metropolit...
6    Nepal PortalAdministrative divisions by country
7  .mw-parser-output .navbar{display:inline;font-...
>>> dfs[1]
                               Municipality                                     Municipality.1
0  Kathmandu, the largest city (Metro city)           Kathmandu, the largest city (Metro city)
1                                  Category                                       Municipality
2                                  Location                                              Nepal
3                                   Created                                      1975 / 08 /16
4                                    Number  6 Metro City11 Sub-metro city276 Urban Municip...
5                               Populations                                    845,767 - 8,370
6                                     Areas  547.43 square kilometres (211.36 sq mi) - 6.89...
7                                Government                                      Mayor–council
8                              Subdivisions                                              Wards
>>> dfs[1]
                               Municipality                                     Municipality.1
0  Kathmandu, the largest city (Metro city)           Kathmandu, the largest city (Metro city)
1                                  Category                                       Municipality
2                                  Location                                              Nepal
3                                   Created                                      1975 / 08 /16
4                                    Number  6 Metro City11 Sub-metro city276 Urban Municip...
5                               Populations                                    845,767 - 8,370
6                                     Areas  547.43 square kilometres (211.36 sq mi) - 6.89...
7                                Government                                      Mayor–council
8                              Subdivisions                                              Wards
>>> dfs[2]
Empty DataFrame
Columns: [(List of Gaupalikas of Nepal, (alternative subdivision))]
Index: []
>>> dfs[3]
   Classification Classification.1               Type  Quantity
0             NaN        Grade 'A'        Very remote        18
1             NaN        Grade 'B'             Remote        43
2             NaN        Grade 'C'  Fairly accessible       133
3             NaN        Grade 'D'         Accessible        99
>>> dfs[4]
   Rank        Name    Nepali  ...        Area (km2) Density (/km2)  Website
0     1   Kathmandu  काठमाडौँ  ...             49.45          17103      [1]
1     2     Pokhara     पोखरा  ...            464.28           1117      [2]
2     3   Bharatpur    भरतपुर  ...            432.95            853      [3]
3     4    Lalitpur   ललितपुर  ...             36.12           8301      [4]
4     5     Birgunj    वीरगंज  ...  132.07[8][9][10]           2031      [5]
5     6  Biratnagar  विराटनगर  ...  77.00[8][11][10]           3179      [6]

[6 rows x 9 columns]
>>> ['Name' in df.columns for df in dfs]
[False,
 False,
 False,
 False,
 True,
 True,
 True,
 True,
 True,
 True,
 True,
 True,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False]
>>> ['Name' in df.columns or 'City' in df.columns for df in dfs]
[False,
 False,
 False,
 False,
 True,
 True,
 True,
 True,
 True,
 True,
 True,
 True,
 True,
 False,
 True,
 True,
 True,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False,
 False]
>>> sum(['Name' in df.columns or 'City' in df.columns for df in dfs])
12
>>> dfs = [df for df in dfs if 'name' in df.columns.str.lower() or 'city' in df.columns.str.lower()]
>>> dfs = [df for df in dfs if 'Name' in df.columns or 'City' in df.columns]
>>> df = pd.concat(dfs)
>>> df['City'].isna().sum()
293
>>> df['Name'].isna().sum()
103
>>> len(df['Name'])
396
>>> 293+103
396
>>> df['Name'].fillna(df['City'][df['Name'].isna()])
>>> name_isna = df.Name.isna()
>>> df['Name'][name_isna] = list(df['City'][name_isna])
>>> df['Name'].loc[name_isna] = list(df['City'][name_isna].copy())
>>> df['Name'].iloc[name_isna] = list(df['City'][name_isna].copy())
>>> names = []
... for i, row in df.iterrows():
...     names.append(row['Name'] if not pd.isna(row['Name']) else row['City'])
...
>>> df['Name'] = names
>>> df['is_city'] = name_isna
>>> df.columns = [str(c).lower().replace(' ', '_').strip() for c in df.columns]
>>> df
    rank           name  ... population_(2011_nepal_census) is_city
0      1      Kathmandu  ...                            NaN   False
1      2        Pokhara  ...                            NaN   False
2      3      Bharatpur  ...                            NaN   False
3      4       Lalitpur  ...                            NaN   False
4      5        Birgunj  ...                            NaN   False
..   ...            ...  ...                            ...     ...
24    25       Tulsipur  ...                        51537.0    True
25    26  Birendranagar  ...                        47914.0    True
26    27     Ratnanagar  ...                        46367.0    True
27    28           Byas  ...                        42899.0    True
28    29        Kalaiya  ...                        42826.0    True

[396 rows x 22 columns]
>>> df.columns = [str(c).lower().replace(' ', '_').replace('(', '').replace(')', '').strip() for c in df.columns]
>>> df
    rank           name  ... population_2011_nepal_census is_city
0      1      Kathmandu  ...                          NaN   False
1      2        Pokhara  ...                          NaN   False
2      3      Bharatpur  ...                          NaN   False
3      4       Lalitpur  ...                          NaN   False
4      5        Birgunj  ...                          NaN   False
..   ...            ...  ...                          ...     ...
24    25       Tulsipur  ...                      51537.0    True
25    26  Birendranagar  ...                      47914.0    True
26    27     Ratnanagar  ...                      46367.0    True
27    28           Byas  ...                      42899.0    True
28    29        Kalaiya  ...                      42826.0    True

[396 rows x 22 columns]
>>> df.columns
Index(['rank', 'name', 'nepali', 'district', 'province', 'population_2021',
       'area_km2', 'density_/km2', 'website', 'area', 'density',
       'population_2011', 'city', 'population_2021[12]', 'population_2011[13]',
       'change_%', 'population_1991_nepal_census', 'households',
       'development_region', 'population_2001_nepal_census',
       'population_2011_nepal_census', 'is_city'],
      dtype='object')
>>> df['population']
>>> df['population'] = df[[c for c in df.columns if 'population_2011' in c]].T.sum()
>>> df[[c for c in df.columns if 'population_2011' in c]]
   population_2011  population_2011[13]  population_2011_nepal_census
0              NaN                  NaN                           NaN
1              NaN                  NaN                           NaN
2              NaN                  NaN                           NaN
3              NaN                  NaN                           NaN
4              NaN                  NaN                           NaN
..             ...                  ...                           ...
24             NaN                  NaN                       51537.0
25             NaN                  NaN                       47914.0
26             NaN                  NaN                       46367.0
27             NaN                  NaN                       42899.0
28             NaN                  NaN                       42826.0

[396 rows x 3 columns]
>>> df[[c for c in df.columns if 'population_2011' in c]].T
                               0    1    2    3   ...       25       26       27       28
population_2011               NaN  NaN  NaN  NaN  ...      NaN      NaN      NaN      NaN
population_2011[13]           NaN  NaN  NaN  NaN  ...      NaN      NaN      NaN      NaN
population_2011_nepal_census  NaN  NaN  NaN  NaN  ...  47914.0  46367.0  42899.0  42826.0

[3 rows x 396 columns]
>>> df[[c for c in df.columns if 'population_2011' in c]].T.sum()
0           0
1           0
2           0
3           0
4           0
       ...   
24    51537.0
25    47914.0
26    46367.0
27    42899.0
28    42826.0
Length: 332, dtype: object
>>> df[[c for c in df.columns if 'population' in c]].T.sum()
0     845767.0
1     518452.0
2     369377.0
3     299843.0
4     268273.0
        ...   
24     51537.0
25     47914.0
26     46367.0
27     42899.0
28     42826.0
Length: 332, dtype: object
>>> population = df[[c for c in df.columns if 'population' in c]].fillna('0').T.sum()
>>> population = df[[c for c in df.columns if 'population' in c]].fillna('0')
>>> population
   population_2021 population_2011  ... population_2001_nepal_census population_2011_nepal_census
0         845767.0               0  ...                            0                            0
1         518452.0               0  ...                            0                            0
2         369377.0               0  ...                            0                            0
3         299843.0               0  ...                            0                            0
4         268273.0               0  ...                            0                            0
..             ...             ...  ...                          ...                          ...
24               0               0  ...                            0                      51537.0
25               0               0  ...                            0                      47914.0
26               0               0  ...                            0                      46367.0
27               0               0  ...                            0                      42899.0
28               0               0  ...                            0                      42826.0

[396 rows x 7 columns]
>>> for c in population.columns:
...     df[c] = [x if np.isnumeric(x) else 0 for x in df[c]]
...
>>> import numpy as np
>>> population.info()
>>> pd.to_numeric(population)
>>> pops = pd.DataFrame()
... for c in population.columns:
...     pops[c] = pd.to_numeric(df[c])
...
>>> pops = pd.DataFrame()
... for c in population.columns:
...     s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...     pops[c] = pd.to_numeric(s)
...
>>> pops = pd.DataFrame()
... for c in population.columns:
...     s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...     s = [np.nan if x == 'nan' else x]
...     pops[c] = pd.to_numeric(s)
...
>>> pops = pd.DataFrame()
... for c in population.columns:
...     s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...     s = [np.nan if x == 'nan' else x for x in s]
...     pops[c] = pd.to_numeric(s)
...
>>> pops
     population_2021  population_2011  ...  population_2001_nepal_census  population_2011_nepal_census
0           845767.0              NaN  ...                           NaN                           NaN
1           518452.0              NaN  ...                           NaN                           NaN
2           369377.0              NaN  ...                           NaN                           NaN
3           299843.0              NaN  ...                           NaN                           NaN
4           268273.0              NaN  ...                           NaN                           NaN
..               ...              ...  ...                           ...                           ...
391              NaN              NaN  ...                           NaN                       51537.0
392              NaN              NaN  ...                           NaN                       47914.0
393              NaN              NaN  ...                           NaN                       46367.0
394              NaN              NaN  ...                           NaN                       42899.0
395              NaN              NaN  ...                           NaN                       42826.0

[396 rows x 7 columns]
>>> pops.T.sum().T
0      845767.0
1      518452.0
2      369377.0
3      299843.0
4      268273.0
         ...   
391     51537.0
392     47914.0
393     46367.0
394     42899.0
395     42826.0
Length: 396, dtype: float64
>>> df['population'] = pops.T.sum().T
>>> df
    rank           name    nepali  ... population_2011_nepal_census is_city  population
0      1      Kathmandu  काठमाडौँ  ...                          NaN   False    845767.0
1      2        Pokhara     पोखरा  ...                          NaN   False    518452.0
2      3      Bharatpur    भरतपुर  ...                          NaN   False    369377.0
3      4       Lalitpur   ललितपुर  ...                          NaN   False    299843.0
4      5        Birgunj    वीरगंज  ...                          NaN   False    268273.0
..   ...            ...       ...  ...                          ...     ...         ...
24    25       Tulsipur       NaN  ...                      51537.0    True    135741.0
25    26  Birendranagar       NaN  ...                      47914.0    True    133327.0
26    27     Ratnanagar       NaN  ...                      46367.0    True    131520.0
27    28           Byas       NaN  ...                      42899.0    True    123316.0
28    29        Kalaiya       NaN  ...                      42826.0    True    121305.0

[396 rows x 23 columns]
>>> df.drop(population.columns)
>>> newcols = [c for c in df.columns if not c.startswith('population_')]
>>> dfnew = df[newcols].copy()
>>> dfnew
    rank           name    nepali   district province  ... change_% households development_region is_city  population
0      1      Kathmandu  काठमाडौँ  Kathmandu  Bagmati  ...      NaN        NaN                NaN   False    845767.0
1      2        Pokhara     पोखरा      Kaski  Gandaki  ...      NaN        NaN                NaN   False    518452.0
2      3      Bharatpur    भरतपुर    Chitwan  Bagmati  ...      NaN        NaN                NaN   False    369377.0
3      4       Lalitpur   ललितपुर   Lalitpur  Bagmati  ...      NaN        NaN                NaN   False    299843.0
4      5        Birgunj    वीरगंज      Parsa  Madhesh  ...      NaN        NaN                NaN   False    268273.0
..   ...            ...       ...        ...      ...  ...      ...        ...                ...     ...         ...
24    25       Tulsipur       NaN        NaN      NaN  ...      NaN    12214.0        Mid-Western    True    135741.0
25    26  Birendranagar       NaN        NaN      NaN  ...      NaN    12029.0        Mid-Western    True    133327.0
26    27     Ratnanagar       NaN        NaN      NaN  ...      NaN    10851.0            Central    True    131520.0
27    28           Byas       NaN        NaN      NaN  ...      NaN    11321.0            Western    True    123316.0
28    29        Kalaiya       NaN        NaN      NaN  ...      NaN     6847.0            Central    True    121305.0

[396 rows x 16 columns]
>>> dfnew.columns
Index(['rank', 'name', 'nepali', 'district', 'province', 'area_km2',
       'density_/km2', 'website', 'area', 'density', 'city', 'change_%',
       'households', 'development_region', 'is_city', 'population'],
      dtype='object')
>>> df = dfnew
>>> df['city']
0               NaN
1               NaN
2               NaN
3               NaN
4               NaN
          ...      
24         Tulsipur
25    Birendranagar
26       Ratnanagar
27             Byas
28          Kalaiya
Name: city, Length: 396, dtype: object
>>> df['name']
0         Kathmandu
1           Pokhara
2         Bharatpur
3          Lalitpur
4           Birgunj
          ...      
24         Tulsipur
25    Birendranagar
26       Ratnanagar
27             Byas
28          Kalaiya
Name: name, Length: 396, dtype: object
>>> df = df.drop('city', axis=1)
>>> df.columns
Index(['rank', 'name', 'nepali', 'district', 'province', 'area_km2',
       'density_/km2', 'website', 'area', 'density', 'change_%', 'households',
       'development_region', 'is_city', 'population'],
      dtype='object')
>>> hist
>>> def combine_numeric_columns(data, prefix):
...     columns = [str(c).lower().strip() data.columns if str(c).lower().strip().startswith(prefix)]
...
>>> def combine_numeric_columns(data, prefix):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...
>>> columns
>>> def combine_numeric_columns(data, prefix):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     return columns
...
>>> combine_numeric_columns(df, 'density')
['density_/km2', 'density']
>>> def combine_numeric_columns(data, prefix):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     return data[columns]
...
>>> combine_numeric_columns(df, 'density')
   density_/km2  density
0         17103      NaN
1          1117      NaN
2           853      NaN
3          8301      NaN
4          2031      NaN
..          ...      ...
24          NaN      NaN
25          NaN      NaN
26          NaN      NaN
27          NaN      NaN
28          NaN      NaN

[396 rows x 2 columns]
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     newdf = getattr(newdf.T, operation)().T
...     return newdf
...
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> combine_numeric_columns(df, 'density')
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.') for x in s]
...         s = [x.replace('.', n=1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> '00.000.00'.replace?
>>> help(str.replace)
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.') for x in s]
...         s = [x.replace('.', count=1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> combine_numeric_columns(df, 'density')
>>> from collections import Counter
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', count=1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> combine_numeric_columns(df, 'density')
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', count=1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> combine_numeric_columns(df, 'density')
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     return pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...
>>> combine_numeric_columns(df, 'density')
0      17103.0
1       1117.0
2        853.0
3       8301.0
4       2031.0
        ...   
391        0.0
392        0.0
393        0.0
394        0.0
395        0.0
Name: density, Length: 396, dtype: float64
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     data = data.copy()
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...     data = data.drop(newdf.columns)
...     data[prefix] = newcol
...     return data
...
>>> combine_numeric_columns(df, 'density')
>>> df.columns
Index(['rank', 'name', 'nepali', 'district', 'province', 'area_km2',
       'density_/km2', 'website', 'area', 'density', 'change_%', 'households',
       'development_region', 'is_city', 'population'],
      dtype='object')
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     data = data.copy()
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...     data = data.drop(list(newdf.columns))
...     data[prefix] = newcol
...     return data
...
>>> combine_numeric_columns(df, 'density')
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     data = data.copy()
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...     for c in  newdf.columns:
...         data = data.drop(c, axis=1)
...     data[prefix] = newcol
...     return data
...
>>> def combine_numeric_columns(data, prefix, operation='sum'):
...     data = data.copy()
...     columns = [str(c).lower().strip() for c in data.columns if str(c).lower().strip().startswith(prefix)]
...     newdf = pd.DataFrame()
...     for c in columns:
...         s = [str(x).replace(',', '').split('[')[0] for x in df[c]]
...         counts = [Counter(x).get('.', 0) for x in s]
...         print(pd.Series(pd.Series(counts) > 1).sum())
...         s = [x.replace('.', '', 1) if count > 1 else x for x, count in zip(s, counts)]
...         s = [np.nan if x == 'nan' else x for x in s]
...         newdf[c] = pd.to_numeric(s)
...     newcol = pd.Series(getattr(newdf.T, operation)().T, name=prefix)
...     data = data.drop(newdf.columns, axis=1)
...     data[prefix] = newcol
...     return data
...
>>> combine_numeric_columns(df, 'density')
    rank           name    nepali   district province  ... households development_region is_city population  density
0      1      Kathmandu  काठमाडौँ  Kathmandu  Bagmati  ...        NaN                NaN   False   845767.0  17103.0
1      2        Pokhara     पोखरा      Kaski  Gandaki  ...        NaN                NaN   False   518452.0   1117.0
2      3      Bharatpur    भरतपुर    Chitwan  Bagmati  ...        NaN                NaN   False   369377.0    853.0
3      4       Lalitpur   ललितपुर   Lalitpur  Bagmati  ...        NaN                NaN   False   299843.0   8301.0
4      5        Birgunj    वीरगंज      Parsa  Madhesh  ...        NaN                NaN   False   268273.0   2031.0
..   ...            ...       ...        ...      ...  ...        ...                ...     ...        ...      ...
24    25       Tulsipur       NaN        NaN      NaN  ...    12214.0        Mid-Western    True   135741.0      0.0
25    26  Birendranagar       NaN        NaN      NaN  ...    12029.0        Mid-Western    True   133327.0      0.0
26    27     Ratnanagar       NaN        NaN      NaN  ...    10851.0            Central    True   131520.0      0.0
27    28           Byas       NaN        NaN      NaN  ...    11321.0            Western    True   123316.0      0.0
28    29        Kalaiya       NaN        NaN      NaN  ...     6847.0            Central    True   121305.0      0.0

[396 rows x 14 columns]
>>> combine_numeric_columns(df, 'density').describe()
             rank     households     population       density
count  396.000000      65.000000     396.000000    396.000000
mean    40.704545   24101.000000  170601.356061   1407.757576
std     40.741744   36075.741539  157081.269469   3239.749827
min      1.000000    6287.000000   42735.000000      0.000000
25%      9.750000   10592.000000   66238.500000      0.000000
50%     24.000000   14271.000000  135741.000000      0.000000
75%     60.250000   20428.000000  198098.000000   1296.000000
max    151.000000  254292.000000  845767.000000  17103.000000
>>> mkdir ~/.nlpia2-data/nepal/
>>> hist -f ~/.nlpia2-data/nepal/clean_wikipedia_list_of_nepal_cities.hist.py
>>> hist -o -p -f ~/.nlpia2-data/nepal/clean_wikipedia_list_of_nepal_cities.hist.md
>>> pwd
'/home/hobs/code/tangibleai/intent-mining'
>>> ls data
>>> hist -o -p -f data/clean_wikipedia_list_of_nepal_cities.hist.md
