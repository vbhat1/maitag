from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import CustomUser, Profile



class UserAdminConfig(BaseUserAdmin):
    """Customizes fields to add/change CustomUser instances"""

    model = CustomUser
    # fields to be used in displaying the CustomUser model
    list_display = [
        'id',
        'email',
        'username',
        'is_active',
        'is_staff',
        'is_superuser'
    ]
    # fields to be used in changing/updating a user
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('username',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser')})
    )
    # fields to be used in creating a new user
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )

    ordering = ('id',)


class ProfileAdmin(admin.ModelAdmin):
    """Customizes fields to add/change Profile instances"""
    
    # fields to be used in displaying the Profile model
    list_display = [
        'user',
        'picture'
    ]


admin.site.register(CustomUser, UserAdminConfig)
admin.site.register(Profile, ProfileAdmin)